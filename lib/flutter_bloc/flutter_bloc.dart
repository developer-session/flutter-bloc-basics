import 'package:bloc_pattern/flutter_bloc/bloc/timer_bloc/timer_bloc.dart';
import 'package:bloc_pattern/flutter_bloc/bloc/timer_bloc/timer_events.dart';
import 'package:bloc_pattern/flutter_bloc/bloc/timer_bloc/timer_states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FlutterBlocDemo extends StatelessWidget {
  final TimerBloc timerBloc = TimerBloc();
  @override
  Widget build(BuildContext context) {
    stream();
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Bloc Demo'),
      ),
      body: Center(
        child: BlocBuilder(
          bloc: timerBloc,
          builder: (context, state) {
            if (state is RunningState) {
              return Text(
                state.tick.toString(),
                style: TextStyle(fontSize: 24),
              );
            } else if (state is ReadyState) {
              return Text('loading');
            } else {
              return Text('Empty State');
            }
          },
        ),
      ),
    );
  }

  stream() {
    Stream<int> stream = Stream.periodic(Duration(seconds: 1), transform);
    stream = stream.take(5);
    stream.listen((data) {
      timerBloc.dispatch(StartTimerEvent(tick: data));
    });
  }

  int transform(int value) {
    return value;
  }
}
