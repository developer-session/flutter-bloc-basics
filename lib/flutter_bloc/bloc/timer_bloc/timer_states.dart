import 'package:bloc_pattern/flutter_bloc/bloc/base/basestate.dart';

class TimerStates extends BaseState {}

class RunningState extends TimerStates {
  int tick;
  RunningState({this.tick});
}

class ReadyState extends TimerStates {}
