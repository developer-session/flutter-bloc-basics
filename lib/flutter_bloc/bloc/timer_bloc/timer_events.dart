import 'package:bloc_pattern/flutter_bloc/bloc/base/base_event.dart';

class TimerEvents extends BaseEvent {}

class StartTimerEvent extends TimerEvents {
  int tick;
  StartTimerEvent({this.tick});
}
