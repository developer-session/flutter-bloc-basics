import 'dart:async';

import 'package:bloc_pattern/flutter_bloc/bloc/timer_bloc/timer_events.dart';
import 'package:bloc_pattern/flutter_bloc/bloc/timer_bloc/timer_states.dart';
import 'package:bloc/bloc.dart';

class TimerBloc extends Bloc<TimerEvents, TimerStates> {
  @override
  TimerStates get initialState => ReadyState();

  @override
  Stream<TimerStates> mapEventToState(TimerEvents event) async* {
    if (event is StartTimerEvent) {
      yield RunningState(tick: event.tick);
    }
  }
}
