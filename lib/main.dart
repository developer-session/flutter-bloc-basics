import 'package:bloc_pattern/flutter_bloc/flutter_bloc.dart';
import 'package:bloc_pattern/SetStateDemo/setstate_demo.dart';
import 'package:bloc_pattern/StreamDemo/stream_demo.dart';
import 'package:flutter/material.dart';

void main() async {
  // Stream<int> stream = Stream.periodic(Duration(seconds: 1), callback).take(6);
  // stream = stream.take(3);
  // await for (var i in stream) {
  //   print(i);
  // }

  runApp(MaterialApp(home: FlutterBlocDemo()));
}

// int callback(int value) {
//   return (value);
// }
