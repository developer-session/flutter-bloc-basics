import 'package:flutter/material.dart';

class SetStateDemo extends StatefulWidget {
  @override
  _SetStateDemoState createState() => _SetStateDemoState();
}

class _SetStateDemoState extends State<SetStateDemo> {
  int counter = 0;
  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    while (counter < 10) {
      print(counter);

      await Future.delayed(Duration(seconds: 1));
      counter++;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    print('build');
    return Scaffold(
      appBar: AppBar(
        title: Text('SetState Demo'),
      ),
      body: Center(
        child: Text(
          counter.toString(),
          style: TextStyle(
            fontSize: 30.0,
          ),
        ),
      ),
    );
  }
}
