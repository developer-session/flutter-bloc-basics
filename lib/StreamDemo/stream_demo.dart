import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class StreamDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Stream Demo'),
        ),
        body: Center(
          child: StreamBuilder(
              stream: stream(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.active) {
                  return Container(
                    child: Text(
                      snapshot.data.toString(),
                      style: TextStyle(fontSize: 24),
                    ),
                  );
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  return Text(
                    'Loading',
                    style: TextStyle(fontSize: 24),
                  );
                } else if (snapshot.connectionState == ConnectionState.done) {
                  return Text(
                    'Done',
                    style: TextStyle(fontSize: 24),
                  );
                } else {
                  return Container();
                }
              }),
        ));
  }

  Stream<int> stream() {
    Stream<int> streamofInt = Stream.periodic(Duration(seconds: 1), transform);
    streamofInt = streamofInt.take(5);
    return streamofInt;
  }

  int transform(int value) {
    return value;
  }
}
